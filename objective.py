# -*- coding: utf-8 -*-
"""
Encapsulates objective functions for TSP
"""
import numpy as np
from euclidean import euclidean_distance
from numba import jit

def symmetric_tour_list(n_cities, start_index = 0):
    """
    Returns a numpy array representiung a symmetric tour of cities
    length = n_cities + 1
    First and last cities are index 'start_index' e.g. for
    start_index = 0 then for 5 cities tour = 
    [0, 1, 2, 3, 4, 0]
    """
    tour = [x for x in range(n_cities)]
    tour.remove(start_index)
    tour.insert(0, start_index)
    tour.append(start_index)
    return np.array(tour)
 
   
def tour_cost_from_matrix(tour, matrix):
    """
    Returns total tour cost
    Euclidean distrances between cities
    are pre-calculated and looked up in an array.
    
    Keyword arguments:
    tour -- numpy array for tour
    matrix -- numpy array of costs in travelling between 2 points.
    """
    cost = 0
    for i in range(len(tour) - 1):
        cost += matrix[tour[i]][tour[i+1]]
        
    return cost
 

def tour_cost(tour, cities, primes):
   """
   Returns total tour cost
   Calculates euclidean distrances on the fly
   (numba doesn't like this function!)
   Runtime = 720ms on average (too slow!)
   
   Keyword arguments:
   tour -- numpy.ndarray containing tour indexes = cities
   cities -- 2d numpy.ndarray containing coordinate pairs for cities
   """
   cost = 0
   
   for i in range(1, len(tour)-1):
      
      base_cost = euclidean_distance(cities[i], cities[i-1])
      
      if i%10 == 0:
         
         if cities[i-1] not in primes:
            
            base_cost *= 1.1
      
      cost += base_cost
   
   return cost
      
   
   
def is_prime_s(primes, city):
   """
   Slightly slower than is_prime_f
   12 micro seconds on average
   """
   if(city in primes):
      return True
   else:
      return False
   

def is_prime_f(primes, city):
   """
   7-8 microseconds on average
   """
   insertion_index = np.searchsorted(primes, [city])
   if(city == primes[insertion_index] or city == primes[insertion_index+1] or city == primes[insertion_index-1]):
      return True
   else:
      return False
    