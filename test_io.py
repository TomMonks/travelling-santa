# -*- coding: utf-8 -*-
"""
Test script for reading of tsp data.
"""

import euclidean as e
import objective as o
import init_solutions as init

import numpy as np


def print_trunc_tour(tour, k):
   """
   Prints a truncated tour.
   I.e. first k cities and last k cities
   """
   print("**Cities: {0}".format(len(tour)))
   print(tour[0:k])
   print('...')
   print(tour[len(tour)-k:len(tour)])


file_path = "data/cities.csv"
primes_path = 'data/primes.csv'
file_out = "data/matrix.csv"
md_rows = 6


#reqd in cities and primes (up to 200k)
cities = np.loadtxt(file_path, skiprows=1, delimiter=',', usecols=(1,2))
primes = np.loadtxt(primes_path, delimiter=',')
print(cities.shape)
print(primes.shape)


#example of calculating a single euclidean distance#
dist = e.euclidean_distance(cities[0], cities[1])
print(dist)

#generate matrix - this causes a memory error with stnd numpy 
#because of the no. cities
#on fly computation or smaller problem instance.
#matrix = e.gen_matrix(cities)
#np.savetxt(file_out, matrix, delimiter=",")

#you can specify the start/end city index using
#the optional parameter start_index.  Default index = 0
print("\n**TOUR DEFAULT BASE = 0")
tour = o.symmetric_tour_list(len(cities)) #  for city 0
print_trunc_tour(tour, 10)


#randomise the cities apart from start/end
tour = init.random_tour(tour)
print("\n**RANDOM TOUR")
print_trunc_tour(tour, 10)


#calcuate tour cost by callin this function
cost = o.tour_cost(tour, cities, primes)
print('*Example cost: {0}'.format(cost))




